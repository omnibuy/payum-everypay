## Usage with Symfony

Please see the [example project](https://bitbucket.org/omnibuy/payum-everypay-example) for usage with Symfony. 

## Running tests

There are currently no tests.

## License

Project is released under the [MIT License](LICENSE).
