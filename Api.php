<?php
namespace Omnibuy\Everypay;

use Http\Message\MessageFactory;
use Payum\Core\Exception\Http\HttpException;
use Payum\Core\HttpClientInterface;

class Api
{
    /**
     * @var HttpClientInterface
     */
    protected $client;

    /**
     * @var MessageFactory
     */
    protected $messageFactory;

    /**
     * @var array
     */
    protected $options = [];

    /**
     * @param array $options
     * @param HttpClientInterface $client
     * @param MessageFactory $messageFactory
     *
     * @throws \Payum\Core\Exception\InvalidArgumentException if an option is invalid
     */
    public function __construct(array $options, HttpClientInterface $client, MessageFactory $messageFactory)
    {
        $this->options = $options;
        $this->client = $client;
        $this->messageFactory = $messageFactory;
    }


    public function createPayment($params) {
        $method = 'POST';
        $url = $this->getApiEndpoint() . '/payments/oneoff';
        $headers = [
            'Authorization' => 'Basic ' . base64_encode($this->options['username'] . ':' . $this->options['secret']),
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        ];
        $body = json_encode($params);

        $request = $this->messageFactory->createRequest($method, $url, $headers, $body);
        $response = $this->client->send($request);

        if (false == ($response->getStatusCode() >= 200 && $response->getStatusCode() < 300)) {
            throw HttpException::factory($request, $response);
        }

        return json_decode((string)$response->getBody()->getContents());
    }

    public function getPaymentInfo($paymentReference, $params)
    {
        $method = 'GET';
        $url = $this->getApiEndpoint() . '/payments/' . $paymentReference . '?' . http_build_query($params);
        $headers = [
            'Authorization' => 'Basic ' . base64_encode($this->options['username'] . ':' . $this->options['secret']),
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        ];

        $request = $this->messageFactory->createRequest($method, $url, $headers);
        $response = $this->client->send($request);

        if (false == ($response->getStatusCode() >= 200 && $response->getStatusCode() < 300)) {
            throw HttpException::factory($request, $response);
        }

        return json_decode($response->getBody()->getContents());
    }

    /**
     * @return string
     */
    private function getApiEndpoint()
    {
        if ($this->options['sandbox']) {
            return 'https://igw-demo.every-pay.com/api/v3';
        } else {
            return 'https://pay.every-pay.eu/api/v3';
        }
    }
}
