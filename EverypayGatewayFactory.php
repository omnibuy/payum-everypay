<?php

namespace Omnibuy\Everypay;

use Omnibuy\Everypay\Action\ConvertPaymentAction;
use Omnibuy\Everypay\Action\CaptureAction;
use Omnibuy\Everypay\Action\NotifyAction;
use Omnibuy\Everypay\Action\StatusAction;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\GatewayFactory;

class EverypayGatewayFactory extends GatewayFactory
{
    /**
     * {@inheritDoc}
     */
    protected function populateConfig(ArrayObject $config)
    {
        $config['payum.default_options'] = [
            'sandbox' => true
        ];
        $config->defaults($config['payum.default_options']);

        $config->defaults([
            'payum.factory_name' => 'everypay',
            'payum.factory_title' => 'EveryPay',
            'payum.action.capture' => new CaptureAction(
                $config['sandbox'],
                $config['username'],
                $config['secret'],
                $config['account_id'],
                $config['default_method']
            ),
            'payum.action.notify' => new NotifyAction(
                $config['sandbox'],
                $config['username'],
                $config['secret'],
                $config['account_id'],
                $config['default_method']
            ),
            'payum.action.status' => new StatusAction(),
            'payum.action.convert_payment' => new ConvertPaymentAction()
        ]);

        $config['payum.required_options'] = [
            'username',
            'secret',
            'account_id'
        ];

        $config->validateNotEmpty($config['payum.required_options']);

        if (false == $config['payum.api']) {
            $config['payum.api'] = function (ArrayObject $config) {
                return new Api((array)$config, $config['payum.http_client'], $config['httplug.message_factory']);
            };
        }
    }
}
