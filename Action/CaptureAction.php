<?php

namespace Omnibuy\Everypay\Action;

use Exception;
use Omnibuy\Everypay\Action\Api\BaseApiAwareAction;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\GatewayAwareTrait;
use Payum\Core\Reply\HttpRedirect;
use Payum\Core\Request\Capture;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\Request\GetHttpRequest;
use Payum\Core\GatewayAwareInterface;
use Payum\Core\Security\GenericTokenFactoryAwareInterface;
use Payum\Core\Security\GenericTokenFactoryAwareTrait;

class CaptureAction extends BaseApiAwareAction implements GatewayAwareInterface, GenericTokenFactoryAwareInterface
{
    use GatewayAwareTrait;
    use GenericTokenFactoryAwareTrait;

    private $isSandbox;
    private $username;
    private $secret;
    private $accountId;
    private $defaultMethod;

    public function __construct(
        $isSandbox,
        $username,
        $secret,
        $accountId,
        $defaultMethod = null
    ) {
        $this->isSandbox = $isSandbox;
        $this->username = $username;
        $this->secret = $secret;
        $this->accountId = $accountId;
        $this->defaultMethod = $defaultMethod;
    }

    /**
     * {@inheritDoc}
     *
     * @param Capture $request
     */
    public function execute($request)
    {
        RequestNotSupportedException::assertSupports($this, $request);

        $model = ArrayObject::ensureArrayObject($request->getModel());
        $token = $request->getToken();

        $getHttpRequest = new GetHttpRequest();
        $this->gateway->execute($getHttpRequest);

        if ($this->isTokenResponse($getHttpRequest)) {
            $paymentReference = $this->parsePaymentReference($getHttpRequest);
            $this->capturePayment($paymentReference, $model);
            return;
        }

        $notifyToken = $this->tokenFactory->createNotifyToken(
            $token->getGatewayName(),
            $token->getDetails()
        );

        $params = [
            'api_username' =>$this->username,
            'account_name' => $this->accountId,
            'amount' => number_format($model['amount'], 2, ".", ""),
            'customer_url' => $token->getTargetUrl(),
            'order_reference' => $model['order_reference'],
            'nonce' => uniqid(true),
            'email' => $model['client_email'],
            'customer_ip' => $getHttpRequest->clientIp,
            'preferred_country' => 'EE', // TODO: do we have this?
            'locale' => 'en', // TODO: do we have this?,
            'timestamp' => (new \DateTime())->format(\DateTime::ATOM),
        ];
        $payment = $this->api->createPayment($params);
        $model['payment_reference'] = $payment->payment_reference;
        $model['notification_url'] = $notifyToken->getTargetUrl();

        $paymentLink = $payment->payment_link;
        if ($this->defaultMethod) {
            foreach ($payment->payment_methods as $paymentMethod) {
                if ($paymentMethod->source !== $this->defaultMethod) {
                    continue;
                }

                $paymentLink = $paymentMethod->payment_link;
            }
        }

        throw new HttpRedirect($paymentLink);
    }

    /**
     * {@inheritDoc}
     */
    public function supports($request)
    {
        return
            $request instanceof Capture &&
            $request->getModel() instanceof \ArrayAccess;
    }

    private function isTokenResponse(GetHttpRequest $request)
    {
        if ($request->method == 'GET' && isset($request->query['payment_reference'])) {
            return true;
        }

        return false;
    }

    private function parsePaymentReference(GetHttpRequest $request)
    {
        if ($request->method == 'GET' && isset($request->query['payment_reference'])) {
            return $request->query['payment_reference'];
        }

        return null;
    }

    private function capturePayment($paymentReference, &$model)
    {
        $params = [
            'api_username' =>$this->username,
            'payment_reference' => $paymentReference
        ];

        try {
            $payment = $this->api->getPaymentInfo($paymentReference, $params);
        } catch (Exception $exception) {
            $model['status'] = 'error';
            $model['error'] = $exception->getMessage();

            return;
        }

        if ($payment->payment_state) {
            $model['status'] = $payment->payment_state;
        }
    }
}
