<?php

namespace Omnibuy\Everypay\Action;

use Payum\Core\Action\ActionInterface;
use Payum\Core\Request\GetStatusInterface;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\Exception\RequestNotSupportedException;

class StatusAction implements ActionInterface
{
    /**
     * {@inheritDoc}
     *
     * @param GetStatusInterface $request
     */
    public function execute($request)
    {
        RequestNotSupportedException::assertSupports($this, $request);

        $model = ArrayObject::ensureArrayObject($request->getModel());

        if (!isset($model['status'])) {
            $request->markNew();
            return;
        } elseif ($model['status'] == 'settled') {
            $request->markCaptured();
            return;
        } elseif ($model['status'] == 'failed') {
            $request->markFailed();
            return;
        } elseif ($model['status'] == 'abandoned') {
            $request->markCanceled();
            return;
        } elseif ($model['status'] == 'refunded') {
            $request->markRefunded();
            return;
        } else {
            $request->markUnknown();
        }
    }

    /**
     * {@inheritDoc}
     */
    public function supports($request)
    {
        return
            $request instanceof GetStatusInterface &&
            $request->getModel() instanceof \ArrayAccess;
    }
}
