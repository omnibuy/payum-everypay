<?php

namespace Omnibuy\Everypay\Action;

use Exception;
use Omnibuy\Everypay\Action\Api\BaseApiAwareAction;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\GatewayAwareTrait;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\Request\GetHttpRequest;
use Payum\Core\GatewayAwareInterface;
use Payum\Core\Request\Notify;
use Payum\Core\Security\GenericTokenFactoryAwareInterface;
use Payum\Core\Security\GenericTokenFactoryAwareTrait;

class NotifyAction extends BaseApiAwareAction implements GatewayAwareInterface, GenericTokenFactoryAwareInterface
{
    use GatewayAwareTrait;
    use GenericTokenFactoryAwareTrait;

    private $isSandbox;
    private $username;
    private $secret;
    private $accountId;
    private $defaultMethod;

    public function __construct(
        $isSandbox,
        $username,
        $secret,
        $accountId,
        $defaultMethod = null
    ) {
        $this->isSandbox = $isSandbox;
        $this->username = $username;
        $this->secret = $secret;
        $this->accountId = $accountId;
        $this->defaultMethod = $defaultMethod;
    }

    /**
     * {@inheritDoc}
     *
     * @param Notify $request
     */
    public function execute($request)
    {
        RequestNotSupportedException::assertSupports($this, $request);

        $model = ArrayObject::ensureArrayObject($request->getModel());

        $getHttpRequest = new GetHttpRequest();
        $this->gateway->execute($getHttpRequest);

        if (!$this->isTokenResponse($getHttpRequest)) {
            return;
        }

        $paymentReference = $this->parsePaymentReference($getHttpRequest);
        $this->capturePayment($paymentReference, $model);
    }

    /**
     * {@inheritDoc}
     */
    public function supports($request)
    {
        return
            $request instanceof Notify &&
            $request->getModel() instanceof \ArrayAccess;
    }

    private function isTokenResponse(GetHttpRequest $request)
    {
        if ($request->method == 'GET' && isset($request->query['payment_reference'])) {
            return true;
        }

        return false;
    }

    private function parsePaymentReference(GetHttpRequest $request)
    {
        if ($request->method == 'GET' && isset($request->query['payment_reference'])) {
            return $request->query['payment_reference'];
        }

        return null;
    }

    private function capturePayment($paymentReference, &$model)
    {
        $params = [
            'api_username' =>$this->username,
            'payment_reference' => $paymentReference
        ];

        try {
            $payment = $this->api->getPaymentInfo($paymentReference, $params);
        } catch (Exception $exception) {
            $model['status'] = 'error';
            $model['error'] = $exception->getMessage();

            return;
        }

        if ($payment->payment_state) {
            $model['status'] = $payment->payment_state;
        }
    }
}
