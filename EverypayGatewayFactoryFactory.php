<?php

namespace Omnibuy\Everypay;

use Payum\Core\GatewayFactoryInterface;

class EverypayGatewayFactoryFactory
{
    public function getCallable()
    {
        return [$this, 'getGatewayFactory'];
    }

    public function getGatewayFactory(array $config, GatewayFactoryInterface $coreGatewayFactory)
    {
        return new EverypayGatewayFactory($config, $coreGatewayFactory);
    }
}
